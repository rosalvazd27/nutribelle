<?php
$url = 'https://profet-prueba.azurewebsites.net/calls/CallPickerWebhook2';

// what post fields?
$fields =array(
    'request_id'=>"4663b977ab2d968ac818dfa9gf14d2bc2146239",
    'call_type'=>"inbound",
    'call_status'=>"Redirected",
    'caller_id'=>"4421275422",
    'date'=>"2017-05-28 17:22:54",
    'duration'=>"2",
    'callpicker_number'=>"524421610188",
    'dialed_number'=>"8116923194",
    'answered_by'=>"",
    'dialed_by'=>"Prueba",
    'record_keys'=> array(
        "4b1d3fa81995586735b234502f5a626d6d85311fa"
    ),
    'city'=>"SANTIAGO DE QUERE2TARO",
    'municipality'=>"QUERETARO",
    'state'=>"QRO"
);

// build the urlencoded data
$postvars = http_build_query($fields);

// open connection
$ch = curl_init();

// set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);

// execute post
$result = curl_exec($ch);
echo $result;
// close connection
curl_close($ch);
