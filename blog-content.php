<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Language" content="es"/>
    <link rel="icon" href="images/favicon.png">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Nutribelle</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<header role="banner" style="z-index:1">
<nav class="navbar navbar-expand-md navbar-fixed-top menu2 main-nav">
        <div class="container">
            <div class="navbar-collapse collapse nav-content order-2">
                <ul class="nav navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link menu-txt2" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu-txt2" href="about.html">Conóceme</a>
                    </li>
                  
                </ul>
            </div>
            <ul class="nav navbar-nav text-nowrap flex-row mx-md-auto order-1 order-md-2">
                <li class="nav-item"><a class="nav-link" href="#"><img src="images/logo-nutribelle.png" class="menu-img"></a></li>
                <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target=".nav-content" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </ul>
            <div class="ml-auto navbar-collapse collapse nav-content order-3 order-md-3">
                <ul class="ml-auto nav navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link menu-txt2" href="services.html">Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu-txt2" href="blog.php">Blog</a>
                    </li>
                  
                </ul>
            </div>
        </div>
    
    </nav>
        </header><!-- header role="banner" -->
    <div class="row">
      
        <div class="offset-md-3 col-md-6 " id="image" style="margin-top:50px; text-align:center">
            
        </div>

        <div class="col-md-12 blogc-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="text-align:center" id="title">

                </div>
                </div>
                <div class="row">  
                    <div class="col-md-12" style="text-align:justify" id="content">
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-12 blogc-4">
            <div class="container">
                    <div class="row">
                            <p class="blogc-subtitle">Los favoritos</p>
                    </div>
                    <div class="row blog-row" id="favourites">  
                       
                </div>
        </div>
        
        
       <div id="footer" class="col-md-12 s6">
        <div class="container">
            <div class="row">
               <div class="col-md-1 footer-box">
                <img src="images/nutibelle-footer.png" class="img-fluid">
               </div>
               <div class="col-md-2 footer-box">
                   <p class="footer-txt"> Aviso de privacidad</p>
                </div>
                <div class="offset-md-7 col-md-2 ">
                    <div style="margin-left: auto; margin-right: auto;">
                 
                        <div >
                                <a href="" target="__blank"><img src="images/facebook.png" class="footer-img"></a>
                                <a href="" target="__blank"><img src="images/instagram.png" class="footer-img"></a>
                        </div>  
                    </div>     
                </div>     
            </div>
                </div>
            </div>
          
            </div>
    </div>
  

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
   
<script>
  $( document ).ready(function() {
            getDetail();
            favourites();
        });

          
        function getDetail(){
            url = document.URL;
            url = String(url.match(/\?+.+/));
            url = url.replace("?", "");
            p = url.split("=");
            var id = p[1];
            
        $.ajax({
             type: "POST",
             url: 'detail.php',
             data: {blog_id : id},
             success: function(data){
                var obj = jQuery.parseJSON(data);

                var title = '<p class="blogc-title">'+obj.title+'</p>';
                $("#title").append(title);
               
                var image = '<img class="img-fluid" src="'+obj.main_image+'"/>';
                $("#image").append(image);
               
                $("#content").html(obj.description);
               
            }
        });
    }

    function favourites(){
            $.ajax({
             type: "POST",
             url: 'favourites.php',
             success: function(data){
                var obj = jQuery.parseJSON(data);
                var data = obj.data;
                
                var cont = 0;
                 var blog_append = "";

                 data.forEach(function(dat){ 
                if(cont < 4){
                  blog_append += '<div class="col-md-3 ">';
                  blog_append += '<img src="'+dat.main_image+'" class="img-fluid blogc-fav">';
                  blog_append += '<p class="blog-text3"><a style="color:black" href="blog-content.php?id='+dat.id+'" >';
                  blog_append += dat.title;            
                  blog_append += '</p></a>';
                  blog_append += '</div>';
                }
                 });

                $("#favourites").append(blog_append);

             }
           });
    
     }

        
    </script>
</body>
</html>