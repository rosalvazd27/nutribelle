<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Language" content="es"/>
    <link rel="icon" href="images/favicon.png">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Nutribelle</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<header role="banner" style="z-index:1">
    <nav class="navbar navbar-expand-md navbar-fixed-top menu2 main-nav">
        <div class="container">
            <div class="navbar-collapse collapse nav-content order-2">
                <ul class="nav navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link menu-txt2" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu-txt2" href="about.html">Conóceme</a>
                    </li>
                  
                </ul>
            </div>
            <ul class="nav navbar-nav text-nowrap flex-row mx-md-auto order-1 order-md-2">
                <li class="nav-item"><a class="nav-link" href="#"><img src="images/logo-nutribelle.png" class="menu-img"></a></li>
                <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target=".nav-content" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </ul>
            <div class="ml-auto navbar-collapse collapse nav-content order-3 order-md-3">
                <ul class="ml-auto nav navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link menu-txt2" href="services.html">Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu-txt2" href="blog.php">Blog</a>
                    </li>
                  
                </ul>
            </div>
        </div>
    
    </nav>
        </header><!-- header role="banner" -->
    <div class="row">
       
        <div class="col-md-12 blog-1 box-flex">
            <div class="container">
                <div class="row">  
                    <div class="col-md-7">
                        <div>
                            <div>
                                <p class="blog-title">Lorem ipsum <br>dolor sit amet</p>
                                <p class="blog-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
                
        <div class="col-md-12 blog-2">
            <div class="container">
                <div class="row">  
                    <div class="col-md-7">
                        <div>
                         <p class="blog-subtitle">Blogs nuevos</p>
                         <div class="separator3"></div>
                        </div>
                        <div id="new_blog">

                        </div>
                    </div>
                    <div class="col-md-5">
                        <div>
                            <p class="blog-subtitle">Favoritos</p>
                            <div class="separator3"></div>
                        </div>
                       <div id="favourites">
                      
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="container">
                <div class="separator2"></div>
            </div>
        </div>
        <div class="col-md-12 blog-3">
            <div class="container">
                <div class="row blog-row" id="complete_blog">
                   
                   
                 
                    <div id="content"></div>
            </div>
            <div class="row blog-row" id="page-selection">
                    
                
                </div>
            </div>
        </div>
        
        
       <div id="footer" class="col-md-12 s6">
        <div class="container">
            <div class="row">
               <div class="col-md-1 footer-box">
                <img src="images/nutibelle-footer.png" class="img-fluid">
               </div>
               <div class="col-md-2 footer-box">
                   <p class="footer-txt"> Aviso de privacidad</p>
                </div>
                <div class="offset-md-7 col-md-2 ">
                    <div style="margin-left: auto; margin-right: auto;">
                 
                        <div >
                                <a href="" target="__blank"><img src="images/facebook.png" class="footer-img"></a>
                                <a href="" target="__blank"><img src="images/instagram.png" class="footer-img"></a>
                        </div>  
                    </div>     
                </div>     
            </div>
                </div>
            </div>
          
            </div>
    </div>
    <input type="hidden" id="totalBlog">


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="js/bootpag.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.4.4/js/tether.min.js"></script>

    <script>
        // init bootpag
        $('#page-selection').bootpag({
            total: 10
        }).on("page", function(event, /* page number here */ num){
            $.ajax({
             type: "POST",
             url: 'lastblog.php',
             success: function(data){
                var obj = jQuery.parseJSON(data);
                var data = obj.data;
                
                var cont = 0;
                 var blog_append = "";
                 $("#totalBlog").val(data.length);

                 data.forEach(function(dat){ 
               
                    var description =  dat.short_description; 
                    var shortDes = description.substr(0,150);
                   
                  blog_append += '<div class="col-md-4">';
                  blog_append += '<img src="'+dat.main_image+'" class="img-fluid"> ';
                  blog_append += '<a href="blog-content.php?id='+dat.id+'" class="blog-text3">"'+dat.title+'"</a>';            
                  blog_append += '</div>';
                 
                
                 });

                $("#content").append(blog_append);

             }
           });
    
         
        });
    </script>
 
<script>
       $( document ).ready(function() {
            lastBlogs();
            favourites();
        });
      
        function favourites(){
            $.ajax({
             type: "POST",
             url: 'favourites.php',
             success: function(data){
                var obj = jQuery.parseJSON(data);
                var data = obj.data;
                
                var cont = 0;
                 var blog_append = "";

                 data.forEach(function(dat){ 
                if(cont < 4){
                    cont++;
                    var description =  dat.short_description; 
                    var shortDes = description.substr(0,150);
                   
                     
                  blog_append += ' <div class="blog-row2">';
                  blog_append += '<p class="blog-subtitle">';
                  blog_append += cont+'. '+dat.title;            
                  blog_append += '</p>';
                  blog_append += '<p class="blog-text2  blog-paddingleft">';
                  blog_append += shortDes;
                  blog_append += '</p>';
                  blog_append += '</div>';
                }
                 });

                $("#favourites").append(blog_append);

             }
           });
    
     }

        function lastBlogs(){
            $.ajax({
             type: "POST",
             url: 'lastblog.php',
             success: function(data){
                var obj = jQuery.parseJSON(data);
                var data = obj.data;
                
                var cont = 0;
                 var blog_append = "";
                 var blog_complete = "";
                 $("#totalBlog").val(data.length);

                 data.forEach(function(dat){ 
                if(cont < 2){
                    cont++;
                    var description =  dat.short_description; 
                    var shortDes = description.substr(0,150);

                  blog_append += '<div class="row blog-row">';
                  blog_append += '<div class="col-md-5 blog-last-left">';
                  blog_append += '<img class="img-fluid" src="'+dat.main_image+'"/>';            
                  blog_append += '</div>';
                  blog_append += '<div class="col-md-7 blog-box-new">';
                  blog_append += '<a href="blog-content.php?id='+dat.id+'" class="blog-subtitle">'+dat.title+'</a>';
                  blog_append += '<p class="blog-text2">'+shortDes+'<br></p>';
                  blog_append += '<a href="blog-content.php?id='+dat.id+'" style="text-decoration: underline" class="blog-text2">Leer mas</a>';
                  blog_append += '</div>';
                  blog_append += '</div>';
                }
                 blog_complete += '<div class="col-md-3">';
                  blog_complete += '<img src="'+dat.main_image+'" class="img-fluid blog-top">';
                  blog_complete += '<p class="blog-text3"><a style="color:black" href="blog-content.php?id='+dat.id+'" >';
                  blog_complete += dat.title;            
                  blog_complete += '</p></a>';
                  blog_complete += '</div>';
                 });

                $("#new_blog").append(blog_append);

                $("#complete_blog").append(blog_complete);

             }
           });
    
     }

  
    </script>
</body>
</html>